Os itens que realizei da avaliação foram:
    a,b,c,d,e,f,g e h.

Descrição item A (USAR REP):
    Particularmente prefiro o gitlab quando se trata de armazenar códigos, pois como o caso desse jogo,
trimestre passado fiz ele e guardei no gitlab para poder excluir do computador, e com os comandos de terminal 
posso simplesmente pergar-lo e atualizar ele quando eu precisar.

Descrição item B (ORGANIZAR REP):
    Não há o que falar sobre este item, é o básico de qualquer projeto deixar-lo organizado para maior facilidade
de navegar entre seus arquivos então é um hábito natural de qualquer programador fazer este item sem precisar explicitar
essa necessidade.

Descrição item C (CONTINUAR):
    Não só porque vale um ponto, mas eu acho mais interessante melhorar este jogo do que começar outro do zero, acho tivertido fazer um tower defense ._. .

Descrição item D e E (criar e usar UPLOAD no jogo):
    Decidi criar um upload para através de um form o usuário mudar a aparencia dos inimigos, imagina você enviar 150 warios para seu oponente?

Descrição item F (EMBELEZAR):
    Usei o bootstrap pra embelezar o minimo da parte web, o paint pra fazer o fundo e o pixrl pra fazer o personagem
principal, de resto foram imagens pegas da internet.

Descrição item G:
    Este arquivo é uma tentativa de um README aceitável, ainda tá meio estranho mas aqui geralmente vão coisas pontuais
sobre o projeto e contextualização dele, então não tem como ter muita coisa mesmo já que é um projeto muito pequeno. Como o professor mostrou, o jogo envia a imagem do usuário pelo front-end com o ajax, recebe pelo backend com flask e registra com sql alchemy o nome da imagem no banco de dados, a única coisa de diferente foi usar o sqlite3 para mecher no banco de dados e pegar a imagem de lá, pois já usava a biblioteca anteriormente para outros assuntos relacionados a banco de dados neste projeto.
    
Descrição item H:
    Em aula só precisei tirar uma dúvida do parametro que passa a imagem para o flask, então só usei uma das aulas para fazer o tralho, o resto fiz em casa.


Pequeno guia para iniciar o jogo:
    1° Abrir o terminal, entrar na pasta web, e executar "python -m http.server"
    2° Em um novo terminal, entrar na pasta web e executar o arquivo backend.py
    3° No navegador, entrar no link http://localhost:8000/enviarInimigos.html
    4° Em um novo terminal, entrar na pasta game e executar o arquivo game.py

Pequena descrição do jogo:
    O jogo tem 2 jogadores, um jogador irá jogar pelo navegador e outro na tela do jogo. 
    
    O jogador do navegador faz parte de um resitência a uma organização chamada Federação que manipula pessoas com ovos, e seu trabalho e enviar inimigos para destruir estes ovos, para isso, deve enviar pelo input quantidades arbitrárias de aliados que farão o trabalho sujo para você. Caso deseje enviar outros tipos de inimigos, basta enviar pelo input do tipo file a imagem do inimigo e os próximos aliados que você enviar terão a nova aparência!

    O jogador que está vendo a tela do jogo é um ovo, e junto das pessoas manipulas deve destruir os inimigos que aparecerem na tela e evitar que os ovos morram! Para chamar aliados basta clicar com o botão esquerdo do mouse no lugar da tela que deseja e ele aparecerá ali. Para movimentar seu personagem, que efetivamente não faz nada mas é bonitinho as animação dele andando, basta usar W A S D.  

