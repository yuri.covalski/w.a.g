import math, pygame,os, manipulandoDb
from pygame.math import Vector2
from random import uniform

class Codigo(pygame.sprite.Sprite):
    def __init__(self, waypoints, image):
        super().__init__()
        self.waypoints = waypoints
        self.spawnTime = pygame.time.get_ticks()
        self.pos = Vector2(self.waypoints[0])
        self.target_wapoint = 1
        self.velocity = 2
        self.angle = 0
        self.imagePath = image
        self.image = pygame.transform.rotate(
            pygame.transform.scale(pygame.image.load(self.imagePath), (95,120)), 
            self.angle)
        self.rect = self.image.get_rect()
        self.rect.center = self.pos
        self.lifes = 3


    def move(self, player):
        if self.target_wapoint < len(self.waypoints):
            self.target = Vector2(self.waypoints[self.target_wapoint])
            self.movement = self.target- self.pos
        else:
            player.life -= 1
            self.kill()

        distance = self.movement.length()
        if distance >= self.velocity:
            self.pos += self.movement.normalize() * self.velocity
        else:
            if distance != 0:
                self.pos += self.movement.normalize() * distance
            self.target_wapoint +=1

    def update(self, player):
        self.move(player)
        self.rotate()

    def rotate(self):
        distance = self.target - self.pos
        self.angle = math.degrees(math.atan2(-distance[1], distance[0]))
        self.image = pygame.transform.rotate(
            pygame.transform.scale(pygame.image.load(self.imagePath), (95,120)), 
            self.angle)
        self.rect = self.image.get_rect()
        self.rect.center = self.pos
    
    def doDamage(self):
        self.lifes -= 1
        if self.lifes == 0:
            self.kill()

def selectDatabaseImage():
    imagem = manipulandoDb.imagemInimigo()
    if imagem == 'vazio':
        caminho = os.path.abspath("game/imagens/codigo.png")
    else:
        caminho = os.path.abspath(f"../web/imagensTorre/{imagem}")
    return caminho


def generateEnemies(sprite_group):
    if len(manipulandoDb.retornaQuantidades()) > 0:
        for quantidade in manipulandoDb.retornaQuantidades():
            for i in range(0,quantidade):
                waypoints = [
                    (uniform(0, 2 * 2.2), uniform(0, 108 * 2.2)),
                    (126 * 2, 110 * 2),
                    (129 * 2.2, 214 * 2.2),
                    (189 * 2.2, 214 * 2.2),
                    (192 * 2.2, 55 * 2.2),
                    (35 * 2.2, 57 * 2.2),
                    (33 * 2.2, 17 * 2.2),
                    (244 * 2.2, 12 * 2.2),
                    (244 * 2.2, 239 * 2.2),
                    (367 * 2.2, 241 * 2.2),
                    (367 * 2.2, 78 * 2.2)
                ]
                code = Codigo(waypoints, selectDatabaseImage())
                sprite_group.add(code)

            manipulandoDb.deletarDoBanco(quantidade)