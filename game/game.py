import pygame, sys, math
from richas import Player
from codigo import generateEnemies


pygame.init()
clock = pygame.time.Clock()

screen_width = 1300
screen_height = 700
screen = pygame.display.set_mode((screen_width,screen_height))
pygame.display.set_caption("W.A.G")

class Forever(pygame.sprite.Sprite):

    def __init__(self, image, pos):
        super().__init__()
        self.image = image
        self.cooldown = 2000
        self.rect = self.image.get_rect()
        self.rect.center = pos
        self.target = None

    def select_target(self,enemy_group):
        x_dist = 0
        y_dist = 0

        for enemy in enemy_group:
            x_dist = enemy.pos[0] - self.rect[0]
            y_dist = enemy.pos[1] - self.rect[1]
            dist = math.sqrt(x_dist ** 2 + y_dist ** 2 )

            if dist < 100:
                self.target = enemy

    def update(self,enemy_group):
        # cooldown inicial de 5 segundos antes das torres começarem a atacar
        if pygame.time.get_ticks() > 5000:
            self.select_target(enemy_group)

        if self.target:
            current_time = pygame.time.get_ticks()
            if current_time - self.target.spawnTime >= self.cooldown:
                self.target.doDamage()
    
            

    def draw(self,surface):
        surface.blit(self.image,self.rect)


cursor_turret_forever = pygame.image.load('imagens/forever.png').convert_alpha()
cursor_turret_forever = pygame.transform.scale(cursor_turret_forever, (200,200))

moving_sprites = pygame.sprite.Group()
player = Player(100,100)
moving_sprites.add(player)

moving_enemies = pygame.sprite.Group()
turret_group = pygame.sprite.Group()


while True:
    fundo = pygame.image.load('imagens/cenario.png')
    screen.blit(fundo, (0,0))

    if player.life == 0:
        print('você perdeu!')
        pygame.quit()
        sys.exit()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        if event.type == pygame.MOUSEBUTTONDOWN:
            posicao = pygame.mouse.get_pos()
            if posicao[0] < screen_width and posicao[1] < screen_height:
                    forever = Forever(cursor_turret_forever, posicao)
                    turret_group.add(forever)

    pk = pygame.key.get_pressed()
    if pk[pygame.K_a]:
        player.rect.x -= player.velocity
        player.walkingLeft()
    if pk[pygame.K_d]:
        player.rect.x += player.velocity
        player.walkingRight()
    if pk[pygame.K_s]:
        player.rect.y += player.velocity
        player.walkingUp()
    if pk[pygame.K_w]:
        player.rect.y -= player.velocity
        player.walkingDown()

    generateEnemies(moving_enemies)

    moving_sprites.update(0.25)
    turret_group.update(moving_enemies)
    moving_enemies.update(player)

    moving_sprites.draw(screen)
    for turret in turret_group:
        turret.draw(screen)
    moving_enemies.draw(screen)


    pygame.display.flip()
    clock.tick(60)