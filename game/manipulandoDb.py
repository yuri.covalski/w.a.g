import sqlite3 as sql

def deletarDoBanco(index):
    con = sql.connect("../web/wave.db")
    cur = con.cursor()
    cur.execute(f'DELETE FROM wave WHERE quantidade = {index};')
    con.commit()
    con.close()
    return 'ok'

def retornaQuantidades():
    quantidadeEntidade = []
    con = sql.connect("../web/wave.db")
    cur = con.cursor()
    for row in cur.execute('select * from wave'):
        quantidadeEntidade.append(row[1])
    con.close()
    return quantidadeEntidade

def imagemInimigo():
    quantidadeEntidade = []
    con = sql.connect("../web/wave.db")
    cur = con.cursor()
    for row in cur.execute('select * from imagem_torre'):
        quantidadeEntidade.append(row[1])
    con.close()
    if len(quantidadeEntidade) == 0:
        return 'vazio'
    else:
        return quantidadeEntidade[-1]
