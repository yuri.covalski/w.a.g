import pygame, os

class Player(pygame.sprite.Sprite):
    def __init__(self, pos_x, pos_y):
        
        super().__init__()
        self.walkRight = False
        self.walkLeft = False
        self.walkUp = False
        self.walkDown = False

        self.sprites = []
        self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasParado.png")))
        self.velocity = 5
        self.current_sprite = 0
        self.image = self.sprites[self.current_sprite]
        self.rect = self.image.get_rect()
        self.rect.topleft = [pos_x,pos_y]
        self.life = 5

    def walkingRight(self):
        self.walkRight = True


    def walkingLeft(self):
        self.walkLeft = True

    def walkingDown(self):
        self.walkDown = True

    def walkingUp(self):
        self.walkUp = True

    def update(self,speed):
        if self.walkRight:
            if len(self.sprites) >= 1:
                self.sprites.clear()
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/direitaParado.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/direita1.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/direitaParado.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/direita2.png")))

            self.current_sprite += speed
            if int(self.current_sprite) >= len(self.sprites):
                self.current_sprite = 0
                self.walkRight = False

        if self.walkLeft:
            if len(self.sprites) > 1:
                self.sprites.clear()
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/esquerdaParado.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/esquerda1.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/esquerdaParado.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/esquerda2.png")))

            self.current_sprite += speed
            if int(self.current_sprite) >= len(self.sprites):
                self.current_sprite = 0
                self.walkLeft = False

        if self.walkUp:
            if len(self.sprites) >= 1:
                self.sprites.clear()
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasParado.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasParado2.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasParado.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasParado.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasParado2.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasParado.png")))

            self.current_sprite += speed
            if int(self.current_sprite) >= len(self.sprites):
                self.current_sprite = 0
                self.walkUp = False
        if self.walkDown:
            if len(self.sprites) >= 1:
                self.sprites.clear()
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasCostas.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasCostas1.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasCostas.png")))
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasCostas1.png")))

            self.current_sprite += speed
            if int(self.current_sprite) >= len(self.sprites):
                self.current_sprite = 0
                self.walkDown = False
        else:
            self.sprites.append(pygame.image.load(os.path.abspath("imagens/richasParado.png")))

        self.image = self.sprites[int(self.current_sprite)]