from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
import os
from flask_cors import CORS
app = Flask(__name__)

CORS(app)  


path = os.path.dirname(os.path.abspath(__file__))
arquivobd = os.path.join(path, 'wave.db')

app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///"+arquivobd
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  
db = SQLAlchemy(app)

#rotas

class Wave(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    quantidade = db.Column(db.Integer)

    def json(self):
        return {
            "id": self.id,
            "quantidade": self.quantidade,
        }
    
class ImagemTorre(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome_foto = db.Column(db.Text)


with app.app_context():
    @app.route("/")
    def inicio():
        return ''
    
    @app.route("/criarInimigos", methods=['POST'])
    def criarInimigos():
        dados = request.get_json()
        inimigo = Wave(**dados)
        db.session.add(inimigo)
        db.session.commit()
    
    @app.route("/salvar_imagem", methods=['POST'])
    def salvar_imagem():
        try:
            f = request.files['nome_foto']
            caminho = os.path.dirname(os.path.abspath(__file__))
            com_pasta = os.path.join(caminho, 'imagensTorre/')
            print(com_pasta)
            completo = os.path.join(com_pasta, f.filename)
            f.save(completo)
            resposta = jsonify({"resultado":"ok", "detalhes": f.filename})
        except Exception as e:
            resposta = jsonify({"resultado":"erro", "detalhes": str(e)})

        return resposta
    
    @app.route("/incluir_imagem", methods=['post'])
    def incluir_pessoa():
        dados = request.get_json(force=True)
        try:
            nova = ImagemTorre(**dados)
            db.session.add(nova) 
            db.session.commit() 
            return jsonify({"resultado": "ok", "detalhes": "oi"})
        except Exception as e:
            return jsonify({"resultado":"erro", "detalhes":str(e)})


    app.run(debug=True, host="0.0.0.0")